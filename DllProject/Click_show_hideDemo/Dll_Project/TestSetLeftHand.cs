﻿using com.ootii.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Slate;

namespace Dll_Project { 
    public class TestSetLeftHand : DllGenerateBase
    {
        public override void Init()
        {
            Debug.Log("TestMessageDispatcher Init !");
        }
        public override void Awake()
        {
            Debug.Log("TestMessageDispatcher Awake !");
        }

        public override void Start()
        {
            BaseMono.ExtralDatas[1].Target.GetComponent<Cutscene>().Play();
            Cutscene.OnCutsceneStopped += TestSetLeftHand_OnStop;
            BaseMono.ExtralDatas[1].Target.GetComponent<Cutscene>().OnSectionReached += sectionreached;
            BaseMono.ExtralDatas[1].Target.GetComponent<Cutscene>().OnGlobalMessageSend += GlobalMessageSend;
            Debug.Log("TestMessageDispatcher Start !");
        }

        void GlobalMessageSend(string str, object obj) {
            Debug.LogWarning(str  + " " + obj.GetType());
        }

        void sectionreached(Section sc) {
            Debug.LogWarning(sc.time + "  " + sc.name);
        }

        private void TestSetLeftHand_OnStop(Cutscene scene)
        {
            scene.Play();
            Debug.LogWarning(scene.name + "Restart");
        }

        public override void OnEnable()
        {
            Debug.Log("TestMessageDispatcher OnEnable !");
        }

        public override void OnDisable()
        {
            Debug.Log("HoffixTestMono OnDisable !");
        }

        public override void Update()
        {
            if (mStaticThings.I != null) {
                BaseMono.ExtralDatas[0].Target.gameObject.transform.position = mStaticThings.I.LeftHand.position;
            }
        }
        public override void OnTriggerEnter(Collider other)
        {
            Debug.LogWarning(other);
        }
    }
}